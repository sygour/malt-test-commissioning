package fr.sgo.malt.test.commissioning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommissioningApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommissioningApplication.class, args);
	}

}
