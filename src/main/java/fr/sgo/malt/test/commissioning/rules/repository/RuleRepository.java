package fr.sgo.malt.test.commissioning.rules.repository;

import fr.sgo.malt.test.commissioning.rules.model.ApiRule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleRepository extends CrudRepository<ApiRule, Long> {
}
