package fr.sgo.malt.test.commissioning.rules.model;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ApiRestrictionsValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiRestrictionsConstraint {
    String message() default "Invalid restriction description";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
