package fr.sgo.malt.test.commissioning.rules.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ApiRule {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private String name;
    @Embedded
    private ApiCommission rate;
    @ApiRestrictionsConstraint
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ApiRestriction> restrictions;
}
