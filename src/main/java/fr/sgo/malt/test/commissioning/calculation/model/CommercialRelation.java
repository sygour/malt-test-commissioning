package fr.sgo.malt.test.commissioning.calculation.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommercialRelation {
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS'Z'")
    private LocalDateTime firstMission;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS'Z'")
    private LocalDateTime lastMission;

    public Duration getDuration() {
        return isFirstTime()
                ? Duration.ZERO
                : Duration.between(firstMission, lastMission);
    }

    public boolean isFirstTime() {
        return firstMission == null && lastMission == null;
    }
}
