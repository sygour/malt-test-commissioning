package fr.sgo.malt.test.commissioning.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class CommissioningUser {
    @Id
    private String username;
    private String password;
    private String roles;
}
