package fr.sgo.malt.test.commissioning.calculation.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiMissionDetails implements WithApiMethods {
    private LocalizableUser client;
    private LocalizableUser freelancer;
    private Mission mission;
    private CommercialRelation commercialRelation;
}
