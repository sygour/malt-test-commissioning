package fr.sgo.malt.test.commissioning.localization.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Location {
    @JsonProperty("country_code")
    private String country;
}
