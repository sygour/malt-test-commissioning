package fr.sgo.malt.test.commissioning.calculation.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Mission {
    @NonNull
    private Long duration;
    @NonNull
    private ChronoUnit unit;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS'Z'")
    private LocalDateTime startDate;

    public Duration getLength() {
        return unit.getDuration().multipliedBy(duration);
    }

    public Duration getDurationToStart() {
        return startDate != null
                ? Duration.between(LocalDateTime.now(), startDate)
                : Duration.ZERO;
    }
}
