package fr.sgo.malt.test.commissioning.calculation.service;

import fr.sgo.malt.test.commissioning.calculation.model.ApiMissionDetails;
import fr.sgo.malt.test.commissioning.rules.model.ApiCommission;

public interface CommissionEngine {
    /**
     * Checks detail against rules and returns matching rate
     *
     * @param details context description
     * @return matching rate
     */
    ApiCommission fireRules(ApiMissionDetails details);
}
