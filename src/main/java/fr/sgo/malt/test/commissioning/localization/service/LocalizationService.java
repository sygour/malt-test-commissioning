package fr.sgo.malt.test.commissioning.localization.service;

import fr.sgo.malt.test.commissioning.localization.model.Location;

public interface LocalizationService {
    Location localize(String ip);
}
