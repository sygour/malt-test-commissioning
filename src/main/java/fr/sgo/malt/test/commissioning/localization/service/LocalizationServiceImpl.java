package fr.sgo.malt.test.commissioning.localization.service;

import fr.sgo.malt.test.commissioning.localization.config.LocalizationConfiguration;
import fr.sgo.malt.test.commissioning.localization.model.Location;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class LocalizationServiceImpl implements LocalizationService {

    private final LocalizationConfiguration configuration;

    public LocalizationServiceImpl(LocalizationConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Location localize(String ip) {
        final String url = String.format(configuration.getUrl(), ip, configuration.getKey());
        final ResponseEntity<Location> geoipResponse = new RestTemplate()
                .getForEntity(url, Location.class);
        return geoipResponse.getBody();
    }
}
