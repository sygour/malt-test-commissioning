package fr.sgo.malt.test.commissioning.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommissioningUserRepository extends CrudRepository<CommissioningUser, String> {
}
