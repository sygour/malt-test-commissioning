package fr.sgo.malt.test.commissioning.security;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Component
public class CommissioningUsersDetailsService implements UserDetailsService {

    private final CommissioningUserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public CommissioningUsersDetailsService(CommissioningUserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findById(username)
                .map(user -> User
                        .withUsername(user.getUsername())
                        .password(user.getPassword())
                        .authorities(Arrays.stream(user.getRoles()
                                .split(":"))
                                .map(SimpleGrantedAuthority::new)
                                .collect(Collectors.toList()))
                        .build())
                .orElseThrow(() -> new UsernameNotFoundException("No user for name " + username));
    }

    @Bean
    public CommandLineRunner initUsers() {
        return args -> {
            userRepository.deleteAll();
            userRepository.save(new CommissioningUser(
                    "user",
                    passwordEncoder.encode("password"),
                    "ROLE_USER"));
            userRepository.save(new CommissioningUser(
                    "manager",
                    passwordEncoder.encode("password"),
                    "ROLE_USER:ROLE_MANAGER"));
        };
    }
}
