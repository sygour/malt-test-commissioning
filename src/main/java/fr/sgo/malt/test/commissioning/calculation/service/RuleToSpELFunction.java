package fr.sgo.malt.test.commissioning.calculation.service;

import fr.sgo.malt.test.commissioning.rules.model.ApiRestriction;
import fr.sgo.malt.test.commissioning.rules.model.ApiRule;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Component;

/**
 * Function that translate a {@link ApiRule} to a SpEL {@link Expression}
 */
@Component
public class RuleToSpELFunction implements RuleToExpressionFunction {

    /**
     * Function that translate a {@link ApiRule} to a SpEL {@link Expression}
     */
    public Expression apply(ApiRule rule) {
        final SpelExpressionParser parser = new SpelExpressionParser();
        return rule.getRestrictions().stream()
                .map(ApiRestriction::toSpELString)
                .reduce((a, b) -> String.format("%s and %s", a, b))
                .map(parser::parseExpression)
                .orElseThrow(() -> new IllegalArgumentException("Restrictions could not be parsed as SpEL expression"));
    }
}
