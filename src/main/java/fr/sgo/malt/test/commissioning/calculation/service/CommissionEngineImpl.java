package fr.sgo.malt.test.commissioning.calculation.service;

import fr.sgo.malt.test.commissioning.calculation.config.CommissionConfiguration;
import fr.sgo.malt.test.commissioning.calculation.model.ApiMissionDetails;
import fr.sgo.malt.test.commissioning.rules.model.ApiCommission;
import fr.sgo.malt.test.commissioning.rules.model.ApiRule;
import fr.sgo.malt.test.commissioning.rules.repository.RuleRepository;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

@Component
public class CommissionEngineImpl implements CommissionEngine {

    private final RuleToExpressionFunction ruleToExpressionFunction;
    private final RuleRepository ruleRepository;
    private final ApiCommission defaultRate;

    public CommissionEngineImpl(RuleToExpressionFunction ruleToExpressionFunction,
                                RuleRepository ruleRepository,
                                CommissionConfiguration commissionConfiguration) {
        this.ruleToExpressionFunction = ruleToExpressionFunction;
        this.ruleRepository = ruleRepository;
        defaultRate = commissionConfiguration.getDefaultRate();
    }

    private Map<ApiRule, Expression> ruleToExpressionMap() {
        final HashMap<ApiRule, Expression> map = new HashMap<>();
        ruleRepository.findAll().forEach(rule -> map.put(
                rule,
                ruleToExpressionFunction.apply(rule)));
        return map;
    }

    @Override
    public ApiCommission fireRules(ApiMissionDetails details) {
        return matchingRules(details).stream().reduce(
                defaultRate,
                BinaryOperator.minBy(Comparator.comparing(ApiCommission::getPercent)));
    }

    private List<ApiCommission> matchingRules(ApiMissionDetails details) {
        return ruleToExpressionMap().entrySet().stream()
                .filter(entry -> evaluate(details, entry))
                .map(entry -> entry.getKey().getRate())
                .collect(Collectors.toList());
    }

    private Boolean evaluate(ApiMissionDetails details, Map.Entry<ApiRule, Expression> entry) {
        Boolean evaluation;
        try {
            evaluation = entry.getValue().getValue(details, Boolean.class);
        } catch (EvaluationException evaluationException) {
            evaluation = Boolean.FALSE;
        }
        return evaluation;
    }
}
