package fr.sgo.malt.test.commissioning.calculation.model;

import fr.sgo.malt.test.commissioning.localization.model.Location;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class LocalizableUser {
    @NonNull
    private String ip;
    private Location location;
}
