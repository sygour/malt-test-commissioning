package fr.sgo.malt.test.commissioning.rules.model;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class ApiRestrictionsValidator implements ConstraintValidator<ApiRestrictionsConstraint, List<ApiRestriction>> {

    @Override
    public boolean isValid(List<ApiRestriction> restrictions, ConstraintValidatorContext context) {
        return restrictions != null
                && !restrictions.isEmpty()
                && restrictions.stream().allMatch(ApiRestriction::isValid);
    }
}
