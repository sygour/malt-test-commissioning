package fr.sgo.malt.test.commissioning.calculation.api;

import fr.sgo.malt.test.commissioning.calculation.model.ApiMissionDetails;
import fr.sgo.malt.test.commissioning.calculation.model.LocalizableUser;
import fr.sgo.malt.test.commissioning.calculation.service.CommissionEngine;
import fr.sgo.malt.test.commissioning.localization.model.Location;
import fr.sgo.malt.test.commissioning.localization.service.LocalizationService;
import fr.sgo.malt.test.commissioning.rules.model.ApiCommission;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommissionController {

    private final CommissionEngine commissionEngine;
    private final LocalizationService localizationService;

    public CommissionController(CommissionEngine commissionEngine, LocalizationService localizationService) {
        this.commissionEngine = commissionEngine;
        this.localizationService = localizationService;
    }

    /**
     * @param details description of the contract and relation client / freelance
     * @return the rate to apply and the rule that caused this rate
     */
    @PostMapping("/commission")
    public ApiCommission calculateCommission(@RequestBody ApiMissionDetails details) {
        details.setClient(localizeUser(details.getClient()));
        details.setFreelancer(localizeUser(details.getFreelancer()));
        return commissionEngine.fireRules(details);
    }

    private LocalizableUser localizeUser(LocalizableUser user) {
        final Location location = localizationService.localize(user.getIp());
        user.setLocation(location);
        return user;
    }
}
