package fr.sgo.malt.test.commissioning.rules.api;

import fr.sgo.malt.test.commissioning.rules.model.ApiRule;
import fr.sgo.malt.test.commissioning.rules.repository.RuleRepository;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RulesController {

    private final RuleRepository ruleRepository;

    public RulesController(RuleRepository ruleRepository) {
        this.ruleRepository = ruleRepository;
    }

    /**
     * @return list containing all the rules
     */
    @GetMapping("/rules")
    public List<ApiRule> listRule() {
        final ArrayList<ApiRule> list = new ArrayList<>();
        ruleRepository.findAll().forEach(list::add);
        return list;
    }

    /**
     * @param id of the rule
     * @return the settings for rule with id
     */
    @GetMapping("/rules/{id}")
    public ApiRule showRule(@PathVariable Long id) {
        return ruleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("No rule for id " + id));
    }

    /**
     * Adds a new rule to calculate commission
     * @param rule description of the rule
     * @return the added rule
     */
    @PostMapping("/rules")
    public ApiRule addRule(@RequestBody @Valid ApiRule rule) {
        rule.getRate().setReason(rule.getName());
        return ruleRepository.save(rule);
    }

}
