package fr.sgo.malt.test.commissioning.calculation.service;

import fr.sgo.malt.test.commissioning.rules.model.ApiRule;
import org.springframework.expression.Expression;

import java.util.function.Function;

/**
 * Function that translate a {@link ApiRule} to an {@link Expression}
 */
public interface RuleToExpressionFunction extends Function<ApiRule, Expression> {
}
