package fr.sgo.malt.test.commissioning.calculation.model;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * Define helper methods for input
 */
public interface WithApiMethods {
    /**
     * @param unit a string defined in {@link ChronoUnit} (eg. MONTHS)
     * @return a duration with value (duration unit, eg. 3 MONTHS)
     */
    default Duration durationOf(Long duration, String unit) {
        return ChronoUnit.valueOf(unit).getDuration().multipliedBy(duration);
    }
}
