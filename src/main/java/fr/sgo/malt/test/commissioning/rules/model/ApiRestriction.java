package fr.sgo.malt.test.commissioning.rules.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.*;
import java.util.List;
import java.util.function.BinaryOperator;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ApiRestriction {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;
    private String attribute;
    private String operator;
    private String value;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ApiRestriction> restrictions;

    /**
     * @return true if all the restrictions are valid (or there is none).<br/>
     * A valid restriction is one of the two:<br/>
     * - a trio of non-null [attribute, operator, value]<br/>
     * - an operator and a non-empty list of restrictions
     */
    @JsonIgnore
    public boolean isValid() {
        return isOperation() ||
                (isComposition() && getRestrictions().stream().allMatch(ApiRestriction::isValid));
    }

    /**
     * translate a restriction in a SpEL string
     *
     * @return SpEL formatted String representing the restriction
     */
    public String toSpELString() {
        if (isOperation()) {
            return operationToSpELString();
        } else if (isComposition()) {
            return compositionToSpELString();
        } else {
            throw new IllegalArgumentException("Restrictions could not be parsed as SpEL expression");
        }
    }

    /**
     * A composition is an operator and a non empty list of restrictions
     */
    private boolean isComposition() {
        return Strings.isNotBlank(getOperator())
                && getRestrictions() != null
                && !getRestrictions().isEmpty();
    }

    /**
     * A simple operation is a non null trio attribute, operator and value
     */
    private boolean isOperation() {
        return Strings.isNotBlank(getOperator())
                && Strings.isNotBlank(getAttribute())
                && Strings.isNotBlank(getValue());
    }

    /**
     * translate operation to SpEL String (ie. "attribute operator value")
     */
    private String operationToSpELString() {
        return concatWithOperator(getOperator())
                .apply(getAttribute(), getValue());
    }

    /**
     * translate composition to SpEL String (eg. "OR [A, B, C]" to "(A OR B OR C)")
     */
    private String compositionToSpELString() {
        return getRestrictions().stream()
                .map(ApiRestriction::toSpELString)
                .reduce(concatWithOperator(getOperator()))
                .map(this::addBrackets)
                .orElseThrow(() -> new IllegalArgumentException("Restrictions could not be parsed as SpEL expression"));
    }

    /**
     * Binary operator to concat simple operation
     *
     * @return BinaryOperator that will return "a operator b"
     */
    private BinaryOperator<String> concatWithOperator(String operator) {
        return (a, b) -> String.format("%s %s %s", a, operator, b);
    }

    /**
     * @return expression surrounded by brackets
     */
    private String addBrackets(String expression) {
        return String.format("(%s)", expression);
    }
}
