package fr.sgo.malt.test.commissioning.calculation.config;

import fr.sgo.malt.test.commissioning.rules.model.ApiCommission;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "commission")
public class CommissionConfiguration {
    private ApiCommission defaultRate;
}
