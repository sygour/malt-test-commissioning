package fr.sgo.malt.test.commissioning.calculation.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sgo.malt.test.commissioning.calculation.model.ApiMissionDetails;
import fr.sgo.malt.test.commissioning.calculation.model.CommercialRelation;
import fr.sgo.malt.test.commissioning.calculation.model.LocalizableUser;
import fr.sgo.malt.test.commissioning.calculation.model.Mission;
import fr.sgo.malt.test.commissioning.localization.model.Location;
import fr.sgo.malt.test.commissioning.rules.model.ApiCommission;
import fr.sgo.malt.test.commissioning.rules.model.ApiRestriction;
import fr.sgo.malt.test.commissioning.rules.model.ApiRule;
import org.junit.jupiter.api.Test;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.io.File;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RuleToSpELFunctionTest {

    private RuleToSpELFunction ruleToSpELFunction = new RuleToSpELFunction();

    @Test
    void shouldTranslateRuleToSpEL() {
        final LocalizableUser client_es = new LocalizableUser("123.123.123.123");
        client_es.setLocation(new Location("ES"));
        final LocalizableUser client_fr = new LocalizableUser("123.123.123.123");
        client_fr.setLocation(new Location("FR"));

        final ApiMissionDetails missionDetails_true = ApiMissionDetails.builder()
                .client(client_es)
                .freelancer(client_es)
                .build();
        final ApiMissionDetails missionDetails_false = ApiMissionDetails.builder()
                .client(client_fr)
                .freelancer(client_fr)
                .build();

        final ApiRule rule = ApiRule.builder()
                .name("one just to rule them all")
                .id(1L)
                .rate(new ApiCommission(8F))
                .restrictions(Stream.of(
                        ApiRestriction.builder().operator("and")
                                .restrictions(Stream.of(
                                        ApiRestriction.builder().attribute("client.location.country").operator("eq").value("'ES'").build(),
                                        ApiRestriction.builder().operator("or")
                                                .restrictions(Stream.of(
                                                        ApiRestriction.builder().attribute("client.location.country").operator("eq").value("'FR'").build(),
                                                        ApiRestriction.builder().attribute("freelancer.location.country").operator("eq").value("'ES'").build()
                                                ).collect(Collectors.toList()))
                                                .build()
                                ).collect(Collectors.toList()))
                                .build(),
                        ApiRestriction.builder().attribute("client.location.country").operator("eq").value("'ES'").build()
                ).collect(Collectors.toList()))
                .build();

        final Expression expression = ruleToSpELFunction.apply(rule);

        assertEquals(Boolean.TRUE, expression.getValue(missionDetails_true));
        assertEquals(Boolean.FALSE, expression.getValue(missionDetails_false));
    }

    @Test
    void shouldCompareMissionDuration() {
        final ApiMissionDetails missionDetails_true = ApiMissionDetails.builder()
                .mission(new Mission(3L, ChronoUnit.MONTHS))
                .build();
        final ApiMissionDetails missionDetails_false = ApiMissionDetails.builder()
                .mission(new Mission(1L, ChronoUnit.MONTHS))
                .build();

        final ApiRule rule = ApiRule.builder()
                .name("one just to rule them all")
                .id(1L)
                .rate(new ApiCommission(8F))
                .restrictions(Stream.of(
                        ApiRestriction.builder().attribute("mission.length").operator("gt").value("durationOf(2, 'MONTHS')").build()
                ).collect(Collectors.toList()))
                .build();

        final Expression expression = ruleToSpELFunction.apply(rule);

        assertEquals(Boolean.TRUE, expression.getValue(missionDetails_true));
        assertEquals(Boolean.FALSE, expression.getValue(missionDetails_false));
    }

    @Test
    void shouldCompareRelationDuration() {
        final ApiMissionDetails missionDetails_true = ApiMissionDetails.builder()
                .commercialRelation(CommercialRelation.builder()
                        .firstMission(LocalDateTime.now().minus(10, ChronoUnit.MONTHS))
                        .lastMission(LocalDateTime.now().minus(6, ChronoUnit.MONTHS))
                        .build())
                .build();
        final ApiMissionDetails missionDetails_false = ApiMissionDetails.builder()
                .commercialRelation(CommercialRelation.builder()
                        .firstMission(LocalDateTime.now().minus(10, ChronoUnit.MONTHS))
                        .lastMission(LocalDateTime.now().minus(9, ChronoUnit.MONTHS))
                        .build())
                .build();

        final ApiRule rule = ApiRule.builder()
                .name("one just to rule them all")
                .id(1L)
                .rate(new ApiCommission(8F))
                .restrictions(Stream.of(
                        ApiRestriction.builder().attribute("commercialRelation.duration").operator("gt").value("durationOf(2, 'MONTHS')").build()
                ).collect(Collectors.toList()))
                .build();

        final Expression expression = ruleToSpELFunction.apply(rule);

        assertEquals(Boolean.TRUE, expression.getValue(missionDetails_true));
        assertEquals(Boolean.FALSE, expression.getValue(missionDetails_false));
    }

    @Test
    void shouldTranslateSpainOrRepeatRule() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        final File file = new File("src/test/resources/01-rule.json");
        final ApiRule rule = mapper.readValue(file, ApiRule.class);

        final Expression expression = ruleToSpELFunction.apply(rule);
        final Expression expected = new SpelExpressionParser().parseExpression(
                "(mission.length gt durationOf(2, 'MONTHS') OR commercialRelation.duration gt durationOf(2, 'MONTHS'))" +
                        " and client.location.country eq 'ES'" +
                        " and freelancer.location.country eq 'ES'");
        assertEquals(expected.getExpressionString(), expression.getExpressionString());
    }
}