package fr.sgo.malt.test.commissioning.rules.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sgo.malt.test.commissioning.rules.model.ApiCommission;
import fr.sgo.malt.test.commissioning.rules.model.ApiRestriction;
import fr.sgo.malt.test.commissioning.rules.model.ApiRule;
import fr.sgo.malt.test.commissioning.rules.repository.RuleRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(RulesController.class)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
class RulesControllerTest {
    @Autowired
    protected MockMvc mockMvc;
    @MockBean
    private RuleRepository mockRuleRepository;
    @Autowired
    private ObjectMapper mapper;

    @Test
    @WithMockUser(roles = {"MANAGER"})
    void shouldAcceptRuleAndReturnSavedRule() throws Exception {
        when(mockRuleRepository.save(any(ApiRule.class))).then(
                (Answer<ApiRule>) invocationOnMock -> invocationOnMock.getArgument(0, ApiRule.class));

        final ApiRule rule = ApiRule.builder()
                .name("some rule")
                .rate(new ApiCommission(8.5F))
                .restrictions(Stream.of(
                        ApiRestriction.builder()
                                .attribute("aaa").operator("bbb").value("ccc")
                                .build()
                ).collect(Collectors.toList()))
                .build();
        final byte[] content = mapper.writeValueAsBytes(rule);

        mockMvc.perform(post("/rules")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .content(content))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("rules_post"));
    }

    @Test
    @WithMockUser
    void shouldReadPreviouslySavedRule() throws Exception {
        final ApiRule rule = ApiRule.builder()
                .name("one just to rule them all")
                .id(1L)
                .rate(new ApiCommission(8F))
                .restrictions(Stream.of(
                        ApiRestriction.builder().operator("or")
                                .restrictions(Stream.of(
                                        ApiRestriction.builder().attribute("mission.duration").operator("gt").value("2months").build(),
                                        ApiRestriction.builder().attribute("commercialrelation.duration").operator("gt").value("2months").build()
                                ).collect(Collectors.toList()))
                                .build(),
                        ApiRestriction.builder().attribute("client.location").operator("eq").value("ES").build()
                ).collect(Collectors.toList()))
                .build();

        when(mockRuleRepository.findById(1L)).thenReturn(Optional.of(rule));

        mockMvc.perform(get("/rules/" + rule.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"id\":1," +
                                "\"name\":\"one just to rule them all\"," +
                                "\"rate\":{\"percent\":8}," +
                                "\"restrictions\":[" +
                                "{\"operator\":\"or\",\"restrictions\":[{\"attribute\":\"mission.duration\",\"operator\":\"gt\",\"value\":\"2months\"},{\"attribute\":\"commercialrelation.duration\",\"operator\":\"gt\",\"value\":\"2months\"}]}," +
                                "{\"attribute\":\"client.location\",\"operator\":\"eq\",\"value\":\"ES\"}]}"))
                .andDo(document("rules_get"));
    }

    @Test
    @WithMockUser
    void shouldListNoRulesWhenStarting() throws Exception {
        mockMvc.perform(get("/rules").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @WithMockUser
    void shouldListPreviouslySavedRules() throws Exception {
        final ApiRule.ApiRuleBuilder builder = ApiRule.builder()
                .name("one just to rule them all")
                .id(1L)
                .rate(new ApiCommission(8F))
                .restrictions(Stream.of(
                        ApiRestriction.builder().attribute("client.location").operator("eq").value("ES").build()
                ).collect(Collectors.toList()));

        final ApiRule firstRule = builder.build();
        final ApiRule secondRule = builder.id(2L).name("yet another one").build();

        when(mockRuleRepository.findAll()).thenReturn(Lists.list(firstRule, secondRule));

        mockMvc.perform(get("/rules")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andDo(document("rules_list"));
    }

    @Test
    @WithMockUser(roles = {"MANAGER"})
    void shouldInvalidateEntryForWrongOperation() throws Exception {
        final String content = "{\"name\":\"one just to rule them all\"," +
                "\"rate\":{\"percent\":8}," +
                "\"restrictions\":[" +
                "{\"attribute\":\"client.location\",\"operator\":\"eq\"}]}";

        mockMvc.perform(post("/rules")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .content(content))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = {"MANAGER"})
    void shouldInvalidateEntryForWrongComposition() throws Exception {
        final String content = "{\"name\":\"one just to rule them all\"," +
                "\"rate\":{\"percent\":8}," +
                "\"restrictions\":[" +
                "{\"operator\":\"or\",\"restrictions\":[]}," +
                "]}";

        mockMvc.perform(post("/rules")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .content(content))
                .andExpect(status().isBadRequest());
    }


    /**
     * Do not use @WithMockUser to test lack of authentication
     */
    @Test
    void shouldRequireAuthenticationToSaveNewRule() throws Exception {
        final String content = "";

        mockMvc.perform(post("/rules")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .content(content))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void shouldRequireManagerRoleToSaveNewRule() throws Exception {
        final String content = "";

        mockMvc.perform(post("/rules")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .content(content))
                .andExpect(status().isForbidden());
    }
}
