package fr.sgo.malt.test.commissioning.calculation.api;

import fr.sgo.malt.test.commissioning.calculation.model.ApiMissionDetails;
import fr.sgo.malt.test.commissioning.calculation.model.CommercialRelation;
import fr.sgo.malt.test.commissioning.calculation.model.LocalizableUser;
import fr.sgo.malt.test.commissioning.calculation.model.Mission;
import fr.sgo.malt.test.commissioning.calculation.service.CommissionEngine;
import fr.sgo.malt.test.commissioning.localization.model.Location;
import fr.sgo.malt.test.commissioning.localization.service.LocalizationService;
import fr.sgo.malt.test.commissioning.rules.model.ApiCommission;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.NoSuchElementException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CommissionController.class)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
class CommissionControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommissionEngine commissionEngine;
    @MockBean
    private LocalizationService localizationService;

    private String stringFromFile(String pathname) throws FileNotFoundException {
        final File file = new File(pathname);
        return new BufferedReader(new FileReader(file))
                .lines()
                .reduce((a, b) -> a + System.lineSeparator() + b)
                .orElseThrow(NoSuchElementException::new);
    }

    @Test
    @WithMockUser
    void shouldFireRulesWithLocationsSet() throws Exception {
        final LocalizableUser client = new LocalizableUser("217.127.206.227");
        client.setLocation(new Location("FR"));
        when(localizationService.localize(client.getIp())).thenReturn(client.getLocation());
        final LocalizableUser freelancer = new LocalizableUser("217.127.206.228");
        freelancer.setLocation(new Location("ES"));
        when(localizationService.localize(freelancer.getIp())).thenReturn(freelancer.getLocation());

        final ApiCommission expectedRate = new ApiCommission(8F);
        expectedRate.setReason("spain or repeat");
        when(commissionEngine.fireRules(any(ApiMissionDetails.class)))
                .thenReturn(expectedRate);

        mockMvc.perform(post("/commission")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .content(stringFromFile("src/test/resources/01-details.json")))
                .andExpect(status().isOk())
                .andExpect(content().json(stringFromFile("src/test/resources/01-result.json")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("commission_post"));

        // verify localization done for client and freelancer
        verify(localizationService, times(1)).localize(eq(client.getIp()));
        verify(localizationService, times(1)).localize(eq(freelancer.getIp()));

        // verify rules were fired with expected details
        final ApiMissionDetails expectedDetails = ApiMissionDetails.builder()
                .client(client)
                .freelancer(freelancer)
                .mission(new Mission(4L, ChronoUnit.MONTHS))
                .commercialRelation(CommercialRelation.builder()
                        .firstMission(LocalDateTime.parse("2018-04-16T13:24:17.510"))
                        .lastMission(LocalDateTime.parse("2018-07-16T14:24:17.510"))
                        .build())
                .build();
        verify(commissionEngine, times(1)).fireRules(eq(expectedDetails));
    }
}