package fr.sgo.malt.test.commissioning;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sgo.malt.test.commissioning.calculation.model.ApiMissionDetails;
import fr.sgo.malt.test.commissioning.localization.model.Location;
import fr.sgo.malt.test.commissioning.localization.service.LocalizationService;
import fr.sgo.malt.test.commissioning.rules.model.ApiCommission;
import fr.sgo.malt.test.commissioning.rules.model.ApiRule;
import fr.sgo.malt.test.commissioning.rules.repository.RuleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.net.URI;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest(
        classes = CommissioningApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class FullIntegrationTests {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private RuleRepository ruleRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private LocalizationService localizationService;

    @BeforeEach
    void setUp() {
        ruleRepository.deleteAll();
        when(localizationService.localize(anyString())).thenReturn(new Location("US"));
    }

    @Test
    void shouldSaveAndRetrieveRules() throws Exception {
        // GET /rules empty at start
        final ResponseEntity<ApiRule[]> emptyListResponse = restTemplate.getForEntity(
                urlForRoute("/rules"),
                ApiRule[].class);
        assertNotNull(emptyListResponse.getBody());
        assertEquals(0, emptyListResponse.getBody().length);

        // POST /rules return saved rule (with id and rate.reason set)
        final ApiRule spainOrRepeatRule = valueFromFile("01-rule.json", ApiRule.class);
        final ResponseEntity<ApiRule> ruleResponse = restTemplate
                .withBasicAuth("manager", "password")
                .postForEntity(
                        urlForRoute("/rules"),
                        spainOrRepeatRule,
                        ApiRule.class);
        assertNotNull(ruleResponse.getBody());
        spainOrRepeatRule.setId(ruleResponse.getBody().getId());
        spainOrRepeatRule.getRate().setReason(spainOrRepeatRule.getName());
        assertEquals(spainOrRepeatRule, ruleResponse.getBody());

        // GET /rules returns 1 element
        final ResponseEntity<ApiRule[]> oneRuleListResponse = restTemplate.getForEntity(
                urlForRoute("/rules"),
                ApiRule[].class);
        assertNotNull(oneRuleListResponse.getBody());
        assertEquals(1, oneRuleListResponse.getBody().length);
        assertEquals(spainOrRepeatRule.getName(), oneRuleListResponse.getBody()[0].getName());

        // GET /rules/:id returns saved rule
        final ResponseEntity<ApiRule> storedRule = restTemplate.getForEntity(
                urlForRoute("/rules/" + spainOrRepeatRule.getId()),
                ApiRule.class);
        assertNotNull(storedRule.getBody());
        assertEquals(spainOrRepeatRule.getName(), storedRule.getBody().getName());
    }

    @Test
    void shouldMatchRuleOrDefault() throws Exception {
        final ApiCommission defaultRate = new ApiCommission(10F);
        defaultRate.setReason("No rule matching - default reason");

        final ApiMissionDetails missionDetails = valueFromFile("01-details.json", ApiMissionDetails.class);
        when(localizationService.localize(anyString())).thenReturn(new Location("ES"));

        // POST /commission when no rule
        final ResponseEntity<ApiCommission> defaultRateResponse = restTemplate
                .withBasicAuth("user", "password")
                .postForEntity(
                        urlForRoute("/commission"),
                        missionDetails,
                        ApiCommission.class);
        assertNotNull(defaultRateResponse.getBody());
        assertEquals(defaultRate, defaultRateResponse.getBody());

        // POST /rules
        final ApiRule spainOrRepeatRule = valueFromFile("01-rule.json", ApiRule.class);
        final ResponseEntity<ApiRule> ruleResponse = restTemplate
                .withBasicAuth("manager", "password")
                .postForEntity(
                        urlForRoute("/rules"),
                        spainOrRepeatRule,
                        ApiRule.class);
        spainOrRepeatRule.getRate().setReason(spainOrRepeatRule.getName());
        assertNotNull(ruleResponse.getBody());

        // POST /commission matching rule
        final ApiCommission expectedRate = valueFromFile("01-result.json", ApiCommission.class);
        final ResponseEntity<ApiCommission> rateResponse = restTemplate
                .withBasicAuth("user", "password")
                .postForEntity(
                        urlForRoute("/commission"),
                        missionDetails,
                        ApiCommission.class);
        assertEquals(expectedRate, rateResponse.getBody());
        assertEquals(spainOrRepeatRule.getRate(), rateResponse.getBody());
    }

    private URI urlForRoute(String route) {
        return URI.create(String.format("http://localhost:%d%s", port, route));
    }

    private <T> T valueFromFile(String filename, Class<T> type) throws Exception {
        return objectMapper.readValue(new File("src/test/resources/" + filename), type);
    }
}
