package fr.sgo.malt.test.commissioning.calculation.service;

import fr.sgo.malt.test.commissioning.calculation.config.CommissionConfiguration;
import fr.sgo.malt.test.commissioning.calculation.model.ApiMissionDetails;
import fr.sgo.malt.test.commissioning.rules.model.ApiCommission;
import fr.sgo.malt.test.commissioning.rules.model.ApiRule;
import fr.sgo.malt.test.commissioning.rules.repository.RuleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
class CommissionEngineTest {

    @MockBean
    private RuleRepository ruleRepository;
    @MockBean
    private RuleToSpELFunction ruleToExpressionFunction;

    private CommissionEngine engine;

    public static final ApiCommission DEFAULT_RATE = new ApiCommission(10F);

    @BeforeEach
    void setUp() {
        DEFAULT_RATE.setReason("default rate");
        final CommissionConfiguration configuration = new CommissionConfiguration(DEFAULT_RATE);
        engine = new CommissionEngineImpl(ruleToExpressionFunction, ruleRepository, configuration);
    }

    @Test
    void shouldFireSingleRuleAndReturnRate() {
        final ApiCommission rate = new ApiCommission(8F);
        rate.setReason("client is in Spain");
        final ApiRule rule = ApiRule.builder()
                .name(rate.getReason())
                .rate(rate)
                .build();

        final ApiMissionDetails mission = ApiMissionDetails.builder().build();

        final Expression expression = mock(Expression.class);
        when(expression.getValue(any(ApiMissionDetails.class), eq(Boolean.class))).thenReturn(Boolean.TRUE);
        when(ruleToExpressionFunction.apply(any(ApiRule.class))).thenReturn(expression);
        when(ruleRepository.findAll()).thenReturn(Stream.of(rule).collect(Collectors.toList()));

        assertEquals(rate, engine.fireRules(mission));
    }

    @Test
    void shouldUseRuleEngineWithStoredRules() {
        final ApiCommission rate = new ApiCommission(8F);
        rate.setReason("client is in Spain");
        final ApiRule rule = ApiRule.builder()
                .name(rate.getReason())
                .rate(rate)
                .build();

        final ApiMissionDetails mission = ApiMissionDetails.builder().build();

        when(ruleRepository.findAll()).thenReturn(Stream.of(rule).collect(Collectors.toList()));
        final Expression expression = mock(Expression.class);
        when(expression.getValue(any(ApiMissionDetails.class), eq(Boolean.class))).thenReturn(Boolean.TRUE);
        when(ruleToExpressionFunction.apply(any(ApiRule.class))).thenReturn(expression);

        setUp();
        assertEquals(rate, engine.fireRules(mission));
    }

    @Test
    void shouldReturnLowestRate() {
        final ApiCommission spainRate = new ApiCommission(8F);
        spainRate.setReason("client is in Spain");
        final ApiRule spainRule = ApiRule.builder()
                .name(spainRate.getReason())
                .rate(spainRate)
                .build();
        final ApiCommission franceRate = new ApiCommission(12F);
        franceRate.setReason("freelancer is in France");
        final ApiRule franceRule = ApiRule.builder()
                .name(franceRate.getReason())
                .rate(franceRate)
                .build();

        final ApiMissionDetails mission = ApiMissionDetails.builder().build();

        final Expression expression = mock(Expression.class);
        when(expression.getValue(any(ApiMissionDetails.class), eq(Boolean.class))).thenReturn(Boolean.TRUE);
        when(ruleToExpressionFunction.apply(any(ApiRule.class))).thenReturn(expression);
        when(ruleRepository.findAll()).thenReturn(Stream.of(spainRule, franceRule).collect(Collectors.toList()));

        assertEquals(spainRate, engine.fireRules(mission));
    }

    @Test
    void shouldReturnDefaultRate() {
        final ApiCommission spainRate = new ApiCommission(8F);
        spainRate.setReason("client is in Spain");
        final ApiRule spainRule = ApiRule.builder()
                .name(spainRate.getReason())
                .rate(spainRate)
                .build();
        final ApiCommission franceRate = new ApiCommission(12F);
        franceRate.setReason("client is in France");
        final ApiRule franceRule = ApiRule.builder()
                .name(franceRate.getReason())
                .rate(franceRate)
                .build();

        final ApiMissionDetails mission = ApiMissionDetails.builder().build();

        final Expression expression = mock(Expression.class);
        when(expression.getValue(any(ApiMissionDetails.class), eq(Boolean.class))).thenReturn(Boolean.FALSE);
        when(ruleToExpressionFunction.apply(any(ApiRule.class))).thenReturn(expression);
        when(ruleRepository.findAll()).thenReturn(Stream.of(spainRule, franceRule).collect(Collectors.toList()));

        assertEquals(DEFAULT_RATE, engine.fireRules(mission));
    }

    @Test
    void shouldSkipRuleIfEvaluationThrowsException() throws Exception {
        final ApiCommission franceRate = new ApiCommission(12F);
        franceRate.setReason("client is in France");
        final ApiRule franceRule = ApiRule.builder()
                .name(franceRate.getReason())
                .rate(franceRate)
                .build();
        final ApiMissionDetails mission = ApiMissionDetails.builder().build();

        final Expression expression = mock(Expression.class);
        when(expression.getValue(any(ApiMissionDetails.class), eq(Boolean.class)))
                .thenThrow(new EvaluationException("Evaluation failed"));
        when(ruleToExpressionFunction.apply(any(ApiRule.class))).thenReturn(expression);
        when(ruleRepository.findAll()).thenReturn(Stream.of(franceRule).collect(Collectors.toList()));

        assertEquals(DEFAULT_RATE, engine.fireRules(mission));
    }
}