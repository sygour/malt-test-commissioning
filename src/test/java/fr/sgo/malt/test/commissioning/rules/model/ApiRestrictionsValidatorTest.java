package fr.sgo.malt.test.commissioning.rules.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ApiRestrictionsValidatorTest {

    private ApiRestrictionsValidator validator;

    @BeforeEach
    void setUp() {
        validator = new ApiRestrictionsValidator();
    }

    @Test
    void shouldInvalidateEmptyRestrictions() throws Exception {
        final List<ApiRestriction> restrictions = new ArrayList<>();

        assertFalse(validator.isValid(restrictions, null));
    }

    @Test
    void shouldValidateSimpleRestrictions() throws Exception {
        final List<ApiRestriction> restrictions = Stream.of(
                ApiRestriction.builder()
                        .attribute("aaa").operator("bbb").value("ccc")
                        .build(),
                ApiRestriction.builder()
                        .attribute("eee").operator("fff").value("ggg")
                        .build()
        ).collect(Collectors.toList());

        assertTrue(validator.isValid(restrictions, null));
    }

    @Test
    void shouldInvalidateSimpleRestrictions() throws Exception {
        final String attribute = "ccc";
        final String operator = "aaa";
        final String value = "bbb";
        final ApiRestriction noAttribute = ApiRestriction.builder()
                .operator(operator).value(value)
                .build();
        final ApiRestriction noOperator = ApiRestriction.builder()
                .attribute(attribute).value(value)
                .build();
        final ApiRestriction noValue = ApiRestriction.builder()
                .attribute(attribute).operator(operator)
                .build();

        assertFalse(validator.isValid(Stream.of(noAttribute).collect(Collectors.toList()), null),
                "expected validation to fail because [attribute] was null");
        assertFalse(validator.isValid(Stream.of(noOperator).collect(Collectors.toList()), null),
                "expected validation to fail because [operator] was null");
        assertFalse(validator.isValid(Stream.of(noValue).collect(Collectors.toList()), null),
                "expected validation to fail because [value] was null");
    }

    @Test
    void shouldValidateComposedRestriction() throws Exception {
        final List<ApiRestriction> restrictions = Stream.of(
                ApiRestriction.builder()
                        .operator("aaa")
                        .restrictions(Stream.of(
                                ApiRestriction.builder()
                                        .attribute("bbb").operator("ccc").value("ddd")
                                        .build()
                        ).collect(Collectors.toList()))
                        .build()
        ).collect(Collectors.toList());

        assertTrue(validator.isValid(restrictions, null));
    }

    @Test
    void shouldInvalidateComposedRestriction() throws Exception {
        final String attribute = "";
        final String operator = "";
        final String value = "";
        final ApiRestriction noOperator = ApiRestriction.builder()
                .restrictions(Stream.of(
                        ApiRestriction.builder()
                                .attribute(attribute).operator(operator).value(value)
                                .build()
                ).collect(Collectors.toList()))
                .build();
        final ApiRestriction noRestrictions = ApiRestriction.builder()
                .operator(operator)
                .build();
        final ApiRestriction invalidNestedRestriction = ApiRestriction.builder()
                .operator(operator)
                .restrictions(Stream.of(
                        ApiRestriction.builder()
                                .build()
                ).collect(Collectors.toList()))
                .build();


        assertFalse(validator.isValid(Stream.of(noOperator).collect(Collectors.toList()), null),
                "expected validation to fail because [operator] was null");
        assertFalse(validator.isValid(Stream.of(noRestrictions).collect(Collectors.toList()), null),
                "expected validation to fail because [restrictions] was null");
        assertFalse(validator.isValid(Stream.of(invalidNestedRestriction).collect(Collectors.toList()), null),
                "expected validation to fail because [nested restriction] was invalid");
    }

    @Test
    void shouldValidateNestedRestrictions() throws Exception {
        final List<ApiRestriction> restrictions = Stream.of(
                ApiRestriction.builder()
                        .operator("aaa")
                        .restrictions(Stream.of(
                                ApiRestriction.builder()
                                        .operator("bbb")
                                        .restrictions(Stream.of(
                                                ApiRestriction.builder()
                                                        .operator("ccc")
                                                        .restrictions(Stream.of(
                                                                ApiRestriction.builder()
                                                                        .operator("ddd")
                                                                        .restrictions(Stream.of(
                                                                                ApiRestriction.builder()
                                                                                        .attribute("eee").operator("fff").value("ggg")
                                                                                        .build()
                                                                        ).collect(Collectors.toList()))
                                                                        .build()
                                                        ).collect(Collectors.toList()))
                                                        .build()
                                        ).collect(Collectors.toList()))
                                        .build()
                        ).collect(Collectors.toList()))
                        .build()
        ).collect(Collectors.toList());

        assertTrue(validator.isValid(restrictions, null));
    }

    @Test
    void shouldValidateSpainOrRepeat() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        final File ruleFile = new File("src/test/resources/01-rule.json");
        final ApiRule rule = mapper.readValue(ruleFile, ApiRule.class);

        assertTrue(validator.isValid(rule.getRestrictions(), null));
    }
}