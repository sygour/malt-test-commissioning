package fr.sgo.malt.test.commissioning.calculation.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ApiMissionDetailsDeserializationTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void shouldDeserializeMissionDetailsCorrectly() throws Exception {
        final ApiMissionDetails expected = ApiMissionDetails.builder()
                .client(new LocalizableUser("217.127.206.227"))
                .freelancer(new LocalizableUser("217.127.206.228"))
                .mission(new Mission(4L, ChronoUnit.MONTHS))
                .commercialRelation(CommercialRelation.builder()
                        .firstMission(LocalDateTime.parse("2018-04-16T13:24:17.510"))
                        .lastMission(LocalDateTime.parse("2018-07-16T14:24:17.510"))
                        .build())
                .build();

        final File file = new File("src/test/resources/01-details.json");
        final ApiMissionDetails apiMissionDetails = objectMapper.readValue(file, ApiMissionDetails.class);

        assertEquals(expected, apiMissionDetails);
    }
}