package fr.sgo.malt.test.commissioning.localization.service;

import fr.sgo.malt.test.commissioning.localization.config.LocalizationConfiguration;
import fr.sgo.malt.test.commissioning.localization.model.Location;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled("Only for external API test")
class LocalizationServiceTest {

    private LocalizationService localizationService;

    @BeforeEach
    void initTestClass() {
        localizationService = new LocalizationServiceImpl(new LocalizationConfiguration(
                "http://api.ipstack.com/%s?access_key=%s",
                "b1c1e639130f23d5e5ce265c823ce58a"));
    }

    @Test
    void shouldCallApiAndReturnLocation() throws Exception {
        final Location location = localizationService.localize("134.201.250.155");

        assertEquals(Location.builder().country("US").build(), location);
    }
}