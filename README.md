# Moteur de règle sur le taux de commissionnement

## Introdution

Le but de ce service est de gérer et appliquer des règles sur le taux de commissionnement d'une mission.

---

## Description de l'API

Les routes disponibles sont décrites ici, les contenu et résultat des requêtes seront décrit dans le paragraphe suivant.

Une documentation générée lors est tests (mvn package) est aussi disponible à `target/getnerated-docs/index.html`.

### Consulter les règles existantes

```
GET /rules
GET /rules/:id                  // id de type long
```

### Ajout d'une nouvelle règle

```
POST /rules
```

### Calcul du taux de commissionnement d'une mission

```
POST /commission
```

---

## Description des données

Les données sont décrites ici, des exemples sont disponibles dans le dossier ```src/test/resources```

### Règle

```
{
  "name": "NOM DE LA REGLE",
  "rate": {
    "percent": POURCENTAGE DE COMMISSION (LONG) 
  },
  "restrictions": [
    LISTE DE RESTRICTION (VOIR APRES)
  ]
}
```

### Restriction

Une restriction peut être de deux formes :

- une restriction simple

```
{
  "attribute": "CLE DE L'ATTRIBUT A VERIFIER", (eg. client.location.country)
  "operator": "OPERATEUR DE COMPARAISON", (eq, gt, lt, etc.)
  "value": "VALEUR ATTENDUE" (entre simples guillements pour les String) 
}
```

- une restriction composée

```
{
  "operator": "OPERATEUR DE COMPOSITION", (eg. or, and)
  "restrictions": [
    UNE LISTE DE RESTRICTION
  ]
}
```

Des méthodes utilitaires peuvent être définies et utiliser dans ces restrictions (et dans les règles).
Ainsi `durationOf(2, 'MONTHS')` permettra de définir une comparaison de durée.

### Mission

Actuellement, une mission est décrite comme suit :

```
{
  "client": {
    "ip": "IP DE CONNEXION DU CLIENT"
  },
  "freelancer": {
    "ip": "IP DE CONNEXION DU FREELANCER"
  },
  "mission": {
    "duration": DUREE DE MISSION,
    "unit": "UNITE DE LA DUREE" (définie dans ChronoUnit : MONTHS, DAYS etc.)
  },
  "commercialRelation": {
    "firstMission": "DATE DE PREMIERE MISSION ENSEMBLE", (format utilisé: yyyy-MM-dd HH:mm:ss.SSS'Z')
    "lastMission": "DATE DE DERNIERE MISSION ENSEMBLE" (format identique)
  }
}
```

### Taux de commissionnement

```
{
  "percent": TAUX DE COMMISSIONNEMENT,
  "reason": "NOM DE LA REGLE VERIFIEE"
}
```

## Démarrer l'application

### Docker compose

L'application est démarrable et arrêtable avec les commandes `docker-compose` suivantes :

```
# Démarrage de la base et de l'application
docker-compose up &
# Arrêt
docker-compose down
```

### Base de données seulement

En développement, il peut être util de ne démarrer que la base :

```
# Démarrage de la base
docker-compose up database
```

### Données de l'application

Les données de la base sont persistées dans le répertoire `.data`, pour les réinitialiser, il suffit de supprimer ce répertoire.

---

## Tests

### Test complet d'intégration

Un test complet (or géolocalisation IP) du service est trouvable dans la classe `FullIntegrationTests` et s'appuie sur les fichiers du répertoire `src/test/resources`.

### Utiliser CURL

```
# enregistrer d'une nouvelle règle
curl -X POST http://localhost:8080/rules \
  --header "Content-Type: application/json" \
  -u manager:password \
  -d @src/test/resources/rule-sample-1.json
# lister toutes les règles
curl -X GET http://localhost:8080/rules
# récupérer le taux de commissionnement pour une mission donnée
curl -X POST http://localhost:8080/commission \
  --header "Content-Type: application/json" \
  -u user:password \
  -d @src/test/resources/details-sample-1.json
```
